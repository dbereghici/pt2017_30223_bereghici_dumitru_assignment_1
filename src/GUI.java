import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

public class GUI extends JFrame {
	private JTextField input1;
	private JTextField input2;
	private JButton btnDeriv1;
	private JButton btnIntegr1;
	private JButton btnDeriv2;
	private JButton btnIntegr2;
	private JButton btnCalc;
	private JTextField output;

	private Polinom op1;
	private Polinom op2;
	private Polinom res;

	public GUI() {
		JPanel content = new JPanel();

		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1, 0));
		p1.add(new JLabel("Introduceti primul polinom"));
		input1 = new JTextField(15);
		input1.setSize(1, 1);
		p1.add(input1);

		JPanel p2 = new JPanel();
		btnDeriv1 = new JButton("Deriveaza");
		btnIntegr1 = new JButton("Integreaza");
		btnDeriv1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				op1 = new Polinom(input1.getText());
				res = op1.derivare();
				output.setText(res.afisare());
			}
		});
		btnIntegr1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				op1 = new Polinom(input1.getText());
				res = op1.integrare();
				output.setText(res.afisare());
			}
		});
		p2.add(btnDeriv1);
		p2.add(btnIntegr1);

		JPanel p3 = new JPanel();
		p3.setLayout(new GridLayout(1, 0));
		p3.add(new JLabel("Introduceti al doilea polinom"));
		input2 = new JTextField(15);
		p3.add(input2);

		JPanel p4 = new JPanel();
		btnDeriv2 = new JButton("Deriveaza");
		btnIntegr2 = new JButton("Integreaza");
		btnDeriv2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				op2 = new Polinom(input2.getText());
				res = op2.derivare();
				output.setText(res.afisare());
			}
		});
		btnIntegr2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				op2 = new Polinom(input2.getText());
				res = op2.integrare();
				output.setText(res.afisare());
			}
		});
		p4.add(btnDeriv2);
		p4.add(btnIntegr2);

		JPanel p5 = new JPanel();
		final JComboBox<String> listaOperatii = new JComboBox<>(new String[] {
				"Adunare", "Scadere", "Inmultire", "Impartire" });

		btnCalc = new JButton("Calculeaza");
		btnCalc.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String s = (String) listaOperatii.getSelectedItem();
				op1 = new Polinom(input1.getText());
				op2 = new Polinom(input2.getText());

				switch (s) {
				case "Adunare":
					res = op1.adunare(op2);
					output.setText(res.afisare());
					break;
				case "Scadere":
					res = op1.scadere(op2);
					output.setText(res.afisare());
					break;
				case "Inmultire":
					res = op1.inmultire(op2);
					output.setText(res.afisare());
					break;
				default:
					if (op2.afisare().equals("0")) {
						JOptionPane.showMessageDialog(null,
								"Eroare : Impartire la 0");
					} else {
						List<Polinom> catRest = op1.impartire(op2);
						output.setText("Cat: " + catRest.get(0).afisare()
								+ " Rest: " + catRest.get(1).afisare());
					}

					break;
				}
			}
		});
		p5.add(listaOperatii);
		p5.add(btnCalc);

		JPanel p6 = new JPanel();
		p6.add(new JLabel("Rezultat:"));
		output = new JTextField(25);
		output.setEditable(false);
		p6.add(output);

		content.add(new JLabel(
				"Introduceti polinoamele sub forma: An^x+An-1x^(n-1)+...+A0"));
		content.add(new JLabel("Ex: 15x^4+20x^3+x+3"));
		content.add(p1);
		content.add(p2);
		content.add(p3);
		content.add(p4);
		content.add(p5);
		content.add(p6);

		this.setContentPane(content);
		this.pack();
		this.setTitle("Operatii pe Polinoame");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocation(500, 200);
		this.setSize(400, 300);
		this.setResizable(false);
	}

	public JTextField getInput1() {
		return input1;
	}

	public JTextField getInput2() {
		return input2;
	}

	public void setOutput(JTextField output) {
		this.output = output;
	}

}
