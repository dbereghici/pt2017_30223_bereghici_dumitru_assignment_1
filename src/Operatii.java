import java.util.ArrayList;

public class Operatii {
	public Polinom adunare(Monom m1, Monom m2) {
		Polinom res = new Polinom();
		res.adaugaMonom(m1);
		res.adaugaMonom(m2);
		return res;
	}

	public Polinom scadere(Monom m1, Monom m2) {
		Polinom res = new Polinom();
		res.adaugaMonom(m1);
		res.adaugaMonom(new Monom(-m2.getCoef(), m2.getGrad()));
		return res;
	}

	public Monom inmultire(Monom m1, Monom m2) {
		return new Monom(m1.getCoef() * m2.getCoef(), m1.getGrad()
				+ m2.getGrad());
	}

	public Monom impartire(Monom m1, Monom m2) {
		return new Monom(m1.getCoef() / m2.getCoef(), m1.getGrad()
				- m2.getGrad());
	}

	public Monom derivare(Monom m) {
		return new Monom(m.getCoef() * m.getGrad(), m.getGrad() - 1);
	}

	public Monom integrare(Monom m) {
		return new Monom(m.getCoef() / (m.getGrad() + 1), m.getGrad() + 1);
	}

	public Polinom adunare(Polinom p1, Polinom p2) {
		Polinom res = new Polinom();
		for (Monom m : p1.getPolinom()) {
			res.adaugaMonom(m);
		}
		for (Monom m : p2.getPolinom()) {
			res.adaugaMonom(m);
		}
		return res;
	}

	public Polinom scadere(Polinom p1, Polinom p2) {
		Polinom res = new Polinom();
		for (Monom m : p1.getPolinom()) {
			res.adaugaMonom(m);
		}
		for (Monom m : p2.getPolinom()) {
			res.adaugaMonom(new Monom(-m.getCoef(), m.getGrad()));
		}
		return res;
	}

	public Polinom inmultire(Polinom p1, Polinom p2) {
		Polinom res = new Polinom();
		Monom m3 = new Monom(0, 0);
		for (Monom m1 : p1.getPolinom()) {
			for (Monom m2 : p2.getPolinom()) {
				m3.setCoef(m1.getCoef() * m2.getCoef());
				m3.setGrad(m1.getGrad() + m2.getGrad());
				res.adaugaMonom(m3);
			}
		}
		return res;
	}

	public Polinom inmultire(Polinom p1, Monom m) {
		Polinom res = new Polinom();
		for (Monom m1 : p1.getPolinom()) {
			res.adaugaMonom(new Monom(m1.getCoef() * m.getCoef(), m1.getGrad()
					+ m.getGrad()));

		}
		return res;
	}

	public ArrayList impartire(Polinom p1, Polinom p2) {
		Polinom res = new Polinom();
		Monom m;
		Polinom aux;
		if (p1.getMaxGrade() >= p2.getMaxGrade()) {
			while (p1.getMaxGrade() >= p2.getMaxGrade()
					&& p1.getMaxGrade() != Integer.MIN_VALUE
					&& p2.getMaxGrade() != Integer.MIN_VALUE) {
				m = p1.getMaxMonom().impartire(p2.getMaxMonom());
				aux = p2.inmultire(m);
				res.adaugaMonom(m);
				p1 = p1.scadere(aux);
				p1.stergeMonoameNule();
			}
		} else {
			res = new Polinom("");
		}
		ArrayList<Polinom> catSiRest = new ArrayList<>();
		catSiRest.add(res);
		catSiRest.add(p1);
		return catSiRest;
		// cat - res ; rest - p1
	}

	public Polinom derivare(Polinom p) {
		Polinom res = new Polinom();
		for (Monom m : p.getPolinom()) {
			res.adaugaMonom(m.derivare());
		}
		return res;
	}

	public Polinom integrare(Polinom p) {
		Polinom res = new Polinom();
		for (Monom m : p.getPolinom()) {
			res.adaugaMonom(m.integrare());
		}
		return res;
	}
}
