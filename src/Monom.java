public class Monom implements Comparable<Monom> {
	private double coef;
	private int grad;
	private Operatii o = new Operatii();

	public Monom(double coef, int grad) {
		this.coef = coef;
		this.grad = grad;
	}

	public double getCoef() {
		return coef;
	}

	public void setCoef(double coef) {
		this.coef = coef;
	}

	public int getGrad() {
		return grad;
	}

	public void setGrad(int grad) {
		this.grad = grad;
	}

	@Override
	public int compareTo(Monom m) {
		return m.getGrad() - this.grad;
	}

	public Polinom adunare(Monom m2) {
		return o.adunare(this, m2);
	}

	public Polinom scadere(Monom m2) {
		return o.scadere(this, m2);
	}

	public Monom inmultire(Monom m2) {
		return o.inmultire(this, m2);
	}

	public Monom impartire(Monom m2) {
		return o.impartire(this, m2);
	}

	public Monom derivare() {
		return o.derivare(this);
	}

	public Monom integrare() {
		return o.integrare(this);
	}

	@Override
	public String toString() {
		if (coef == -1) {
			if (grad == 0) {
				return "-1" + "+";
			} else if (grad == 1) {
				return "-x" + "+";
			} else {
				return "-x^" + grad + "+";
			}
		} else if (coef == 1) {
				if (grad == 0) {
					return "1" + "+";
				} else if (grad == 1) {
					return "x" + "+";
				} else {
					return "x^" + grad + "+";
				}
		} else {
			if (grad == 0) {
				if (coef == (int) coef) {
					return (int)coef + "+";
				} else {
					return String.format("%.2f", coef)+ "+";
				}
			} else if (grad == 1) {
				if (coef == (int) coef) {
					return (int)coef + "x" + "+";
				} else {
					return String.format("%.2f", coef) + "x" + "+";
				}
			} else {
				if (coef == (int) coef) {
					return (int)coef + "x^" + grad + "+";
				} else {
					return String.format("%.2f", coef) + "x^" + grad + "+";
				}
			}
		}
	}
}
