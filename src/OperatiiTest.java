import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class OperatiiTest {

	@Test
	public void test_adunare1() {
		Polinom p1 = new Polinom("4x^4-3x^5-2x^2-1+x");
		Polinom p2 = new Polinom("4x^12+x^5+2x^2-172x^3+x");
		Polinom res = p1.adunare(p2);
		assertEquals("4x^12-2x^5+4x^4-172x^3+2x-1", res.afisare());
	}

	@Test
	public void test_adunare2() {
		Polinom p1 = new Polinom("4x^4-3x^5-2x^2-1+x");
		Polinom p2 = new Polinom("1-x+3x^5-4x^4+2x^2");
		Polinom res = p1.adunare(p2);
		assertEquals("0", res.afisare());
	}

	@Test
	public void test_scadere1() {
		Polinom p1 = new Polinom("4x^4-3x^5-2x^2-1+x");
		Polinom p2 = new Polinom("4x^12+x^5+2x^2-172x^3+x");
		Polinom res = p1.scadere(p2);
		assertEquals("-4x^12-4x^5+4x^4+172x^3-4x^2-1", res.afisare());
	}

	@Test
	public void test_scadere2() {
		Polinom p1 = new Polinom("0");
		Polinom p2 = new Polinom("1-x+3x^5-4x^4+2x^2-x");
		Polinom res = p1.scadere(p2);
		assertEquals("-3x^5+4x^4-2x^2+2x-1", res.afisare());
	}

	@Test
	public void test_inmultire1() {
		Polinom p1 = new Polinom("4x^4-1");
		Polinom p2 = new Polinom("4x^12-172x^3+x");
		Polinom res = p1.inmultire(p2);
		assertEquals("16x^16-4x^12-688x^7+4x^5+172x^3-x", res.afisare());
	}

	@Test
	public void test_inmultire2() {
		Polinom p1 = new Polinom("0");
		Polinom p2 = new Polinom("1-x+3x^5-4x^4+2x^2-x");
		Polinom res = p1.inmultire(p2);
		assertEquals("0", res.afisare());
	}

	@Test
	public void test_impartire1() {
		Polinom p1 = new Polinom("4x^12-172x^3+x");
		Polinom p2 = new Polinom("4x^4-1");
		ArrayList<Polinom> catSiRest = p1.impartire(p2);
		assertEquals("x^8+0,25x^4+0,06", catSiRest.get(0).afisare());
		assertEquals("-172x^3+x+0,06", catSiRest.get(1).afisare());
	}

	@Test
	public void test_impartire2() {
		Polinom p1 = new Polinom("0");
		Polinom p2 = new Polinom("1-x+3x^5-4x^4+2x^2-x");
		ArrayList<Polinom> catSiRest = p1.impartire(p2);
		assertEquals("0", catSiRest.get(0).afisare());
		assertEquals("0", catSiRest.get(1).afisare());
	}

	@Test
	public void derivare1() {
		Polinom p = new Polinom("1-x+3x^5-4x^4+2x^2-x");
		Polinom res = p.derivare();
		assertEquals("15x^4-16x^3+4x-2", res.afisare());
	}

	@Test
	public void derivare2() {
		Polinom p = new Polinom("15");
		Polinom res = p.derivare();
		assertEquals("0", res.afisare());
	}

	@Test
	public void integrare1() {
		Polinom p = new Polinom("1-x+3x^5-4x^4+2x^2-x");
		Polinom res = p.integrare();
		assertEquals("0,50x^6-0,80x^5+0,67x^3-x^2+x", res.afisare());
	}

	@Test
	public void integrare2() {
		Polinom p = new Polinom("15");
		Polinom res = p.integrare();
		assertEquals("15x", res.afisare());
	}

}
