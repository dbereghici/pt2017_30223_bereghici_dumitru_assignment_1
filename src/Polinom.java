import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JOptionPane;

public class Polinom {
	private List<Monom> polinom = new ArrayList<>();
	private int maxGrade;
	private Monom maxMonom;
	private Operatii o = new Operatii();

	public Polinom() {
	}

	public Polinom(String s) {
		initPolinom(s);
		sortPolinom();
		if (!polinom.isEmpty()) {
			maxMonom = polinom.get(0);
			maxGrade = polinom.get(0).getGrad();
		}
		stergeMonoameNule();
	}

	public void sortPolinom() {
		Collections.sort(polinom);
	}

	public void adaugaMonom(Monom m) {
		boolean gasit = false;
		for (Monom m1 : polinom) {
			if (m1.getGrad() == m.getGrad() && !gasit) {
				m1.setCoef(m.getCoef() + m1.getCoef());
				gasit = true;
			}
		}
		if (!gasit) {
			polinom.add(new Monom(m.getCoef(), m.getGrad()));
		}
	}

	public void initPolinom(String s) {
		if (s.equals("")) {
			return;
		}
		s = s.replaceAll("-", "+-");
		if (s.startsWith("+")) {
			s = s.substring(1);
		}
		for (String s1 : s.split("\\+")) {
			if (s1.startsWith("x")) {
				s1 = "1" + s1;
			}
			if (s1.startsWith("-x")) {
				s1 = "-1" + s1.substring(1);
			}
			if (!s1.contains("x")) {
				s1 = s1 + "x^0";
			}
			if (s1.endsWith("x")) {
				s1 = s1 + "^1";
			}
			if (!s1.contains("^")) {
				JOptionPane.showMessageDialog(null,
						"Nu ati respectat formatul cerut");
			}
			String[] coefAndGrad = s1.split("x");
			coefAndGrad[1] = coefAndGrad[1].substring(1,
					coefAndGrad[1].length());
			try {
				Integer i1 = Integer.parseInt(coefAndGrad[0]);
				Integer i2 = Integer.parseInt(coefAndGrad[1]);
				adaugaMonom(new Monom(i1, i2));
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null,
						"Nu ati respectat formatul cerut");
			}
		}
	}

	public void stergeMonoameNule() {
		ArrayList<Monom> polinomAux = new ArrayList<Monom>(polinom);
		polinom.clear();
		for (Monom m : polinomAux) {
			if (m.getCoef() != 0) {
				polinom.add(m);
			}
		}
	}

	public String afisare() {
		stergeMonoameNule();
		sortPolinom();
		if (polinom.isEmpty()) {
			return "0";
		}

		String s = "";
		for (Monom m : polinom) {
			s = s + m.toString();
		}

		if (s.equals("")) {
			return "";
		}
		s = s.substring(0, s.length() - 1);
		s = s.replaceAll("\\+-", "-");

		return s;
	}

	public List<Monom> getPolinom() {
		return polinom;
	}

	public void setPolinom(ArrayList<Monom> polinom) {
		this.polinom = polinom;
	}

	public int getMaxGrade() {
		Collections.sort(polinom);
		if (polinom.isEmpty()) {
			return Integer.MIN_VALUE;
		}
		Monom m = polinom.get(0);
		this.maxGrade = m.getGrad();
		return maxGrade;
	}

	public Monom getMaxMonom() {
		Collections.sort(polinom);
		if (polinom.isEmpty()) {
			return null;
		}
		maxMonom = polinom.get(0);
		return maxMonom;
	}

	public Polinom adunare(Polinom p2) {
		return o.adunare(this, p2);
	}

	public Polinom scadere(Polinom p2) {
		return o.scadere(this, p2);
	}

	public Polinom inmultire(Polinom p2) {
		return o.inmultire(this, p2);
	}

	public Polinom inmultire(Monom m) {
		return o.inmultire(this, m);
	}

	public ArrayList impartire(Polinom p2) {
		return o.impartire(this, p2);
	}

	public Polinom derivare() {
		return o.derivare(this);
	}

	public Polinom integrare() {
		return o.integrare(this);
	}

}
